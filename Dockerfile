FROM ruby:2.3.1

WORKDIR /usr/src/app
COPY Gemfile* ./
RUN bundle install
COPY . .

EXPOSE 4567
CMD ["puma", "-b", "tcp://0.0.0.0:4567"]
