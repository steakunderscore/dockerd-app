# This file is used by Rack-based servers to start the application.
$:.unshift File.dirname(__FILE__)

require 'bundler/setup'
require 'lib/dockered-app'

run Sinatra::Application
