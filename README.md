# Dockered App

Me playing with docker for learning sake.

## Updating the docker container

Use the `Dockerfile` to build the docker container.
```bash
docker build -t dockered-app .
```

## Running

Use `docker run` to run the app!

For _development_, override the apps src directory.
```bash
docker run -v $PWD:/usr/src/app --name development-dockered-app -p 8080:4567 --rm dockered-app
```

For _production_ set `RACK_ENV` and push to the background.
```bash
docker run -e RAILS_ENV=production -e RACK_ENV=production --name production-dockered-app -p 8080:4567 -d dockered-app
```
